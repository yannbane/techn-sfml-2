#ifndef TECHN_IMAGEMANAGER_H_
#define TECHN_IMAGEMANAGER_H_

#include <SFML/Graphics.hpp>
#include <map>

class ImageManager
{
    public:

    //Doesn't load the images:
    sf::Sprite *GetSprite(std::string name);
    sf::Texture *GetImage(std::string name);
    //Loads the image, returns success:
    bool LoadImage(std::string filename, std::string name);


    private:

    std::map<std::string, std::vector< sf::Texture*>* > images_;
};

#endif
