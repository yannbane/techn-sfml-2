#include "twindow.hpp"

Twindow::Twindow() :
    vsync_(true),
    style_(-1)
{
}

void Twindow::UpdateWindow()
{
    if (style_ != -1) {
        create(sf::VideoMode(width_, height_, bitdepth_), title_, style_);
    }
    else {
        create(sf::VideoMode(width_, height_, bitdepth_), title_);
    }

    setVerticalSyncEnabled(vsync_); //This needs to be a setting.
}

void Twindow::set_width_height(int width, int height)
{
    width_ = width;
    height_ = height;
}

void Twindow::TakeScreenshot()
{
    sf::Image screen = capture();
    //string home = getEnv("HOME");
    screen.saveToFile("screenshot.png");
}
