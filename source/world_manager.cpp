#include "world_manager.hpp"

#include "world.hpp"
#include "image_manager.hpp"

void WorldManager::Load(
    std::string filename,
    World *world,
    ImageManager *im)
{
    /* 1. Read pics from file (filename, name pairs).
     * 2. Load all the pics.
     * 3. Read ground polygon, construct a body from it.
     * 4. Read objects, construct Objects from them, create their
     *    bodies, add them to the world.
     * 6. Read decorations, create and apply them.
     */
}
