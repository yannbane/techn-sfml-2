#ifndef TECHN_TWINDOW_H_
#define TECHN_TWINDOW_H_

#include <string>

#include <SFML/Graphics.hpp>

class Twindow : public sf::RenderWindow
{
    public:

    Twindow();

    //Call UpdateWindow after these:
    void set_bitdepth(unsigned short bitdepth){bitdepth_ = bitdepth;}
    void set_width_height(int width, int height);
    void set_width(int width){width_ = width;}
    void set_height(int height){height_ = height;}
    void set_title(std::string title){title_ = title;}
    void set_vsync(bool vsync){vsync_ = vsync;}
    void set_style(int style){style_ = style;}

    void UpdateWindow();
    void TakeScreenshot();


    private:

    bool vsync_;
    int width_;
    int height_;
    int bitdepth_;
    int style_;
    std::string title_;
};

#endif
