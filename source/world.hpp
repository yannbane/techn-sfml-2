#ifndef TECHN_WORLD_H_
#define TECHN_WORLD_H_

#include <vector>
#include <string>

#include <Box2D/Box2D.h>

class Object;

class World
{
    public:

    World();

    //Get the number of objects:
    int NumObjects(){return objects_.size();};
    //Get an object by index:
    Object *objecti(int i){return objects_[i];}
    //Get the first object with the name 'name':
    Object *objectn(std::string name);
    //Return the pointer to the object vector:
    std::vector<Object*>* objects(){return &objects_;}
    //Return the pointer to the world_ object:
    b2World* world(){return &world_;}

    void set_velocity_it(unsigned int it){velocity_it_ = it;}
    void set_position_it(unsigned int it){position_it_ = it;}

    //Updates physics and sprite positions.
    void Update(float dt); //dt is in seconds
    //Adds to first nullptr
    void AddObject(Object *ob);
    //Doesn't delete the object
    void RemoveObjecti(int index);
    //Deletes the object and removes it
    void DeleteObjecti(int index);
    //Remove ALL objects with name 'name':
    void RemoveObjectn(std::string name);
    void DeleteObjectn(std::string name);


    private:

    //Creates the object's body and other attributes:
    void PrepareObject(Object* ob);

    std::vector<Object*> objects_;
    b2Vec2 gravity_;
    b2World world_;
    unsigned int velocity_it_;
    unsigned int position_it_;

    void UpdateObject(Object* ob);
};

#endif
