#include <stdlib.h>

#include "image_manager.hpp"

#include "tinclude.hpp"

bool ImageManager::LoadImage(std::string filename, std::string name)
{
    sf::Texture* image = new sf::Texture;

    if (!image->loadFromFile(filename)) {
        return false;
    }

    //If this is the first image with that name:
    if (images_.find(name) == images_.end()) {
         //Create image the vector.
        images_[name] = new std::vector<sf::Texture*>;
    }

    images_[name]->push_back(image);

    return true;
}

sf::Texture* ImageManager::GetImage(std::string name)
{
    if (images_.find(name) != images_.end()) {
        unsigned int index = rand()%images_[name]->size();
        return (*images_[name])[index];
    }

    return nullptr;
}

sf::Sprite* ImageManager::GetSprite(std::string name)
{
    if (images_.find(name) != images_.end()) {
        sf::Sprite* sprite = new sf::Sprite;
        sf::Texture*  image  = GetImage(name);

        sprite->setTexture(*image);
        return sprite;
    }
    else {
        return nullptr;
    }
}
