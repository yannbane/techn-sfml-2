#include "renderer.hpp"

Renderer::Renderer()
{
    need_screenshot_ = false; //Automatically reset after a screenshot.

    sf::Vector2f center = rW_.GetDefaultView().GetCenter();
    world_view_.SetCenter(center);
    sf::Vector2f halfSize = rW_.GetDefaultView().GetHalfSize();
    world_view_.SetHalfSize(halfSize);
}

void Renderer::UpdateWindow()
{
    rW_.Create(sf::VideoMode(width_, height_, bitdepth_), title_);
    rW_.UseVerticalSync(vsync_); //This needs to be a setting.
}

void Renderer::set_width_height(int width, int height)
{
    width_ = width;
    height_ = height;
}

void Renderer::Quit()
{
    rW_.Close();
}

void Renderer::RenderWorld()
{
    rW_.SetView(world_view_);
    rW_.Clear(sf::Color(210, 215, 255));

    /*for (int i = 0; i < world_.NumObjects(); ++i)
    {
        rW_.Draw(*world_.objecti(i)->sprite());
    }*/
}

void Renderer::RenderUI()
{
    rW_.SetView(rW_.GetDefaultView());
}

void Renderer::Render()
{
    rW_.Clear();

    RenderWorld();
    RenderUI();

    rW_.Display();

    if (need_screenshot_)
    {
        TakeScreenshot();
    }
}

void Renderer::TakeScreenshot()
{
    need_screenshot_ = false;
    sf::Image screen = rW_.Capture();
    //string home = getEnv("HOME");
    screen.SaveToFile("screenshot.png");
}
