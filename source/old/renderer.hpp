#ifndef TECHN_RENDERER_H_
#define TECHN_RENDERER_H_

#include <string>

#include <SFML/Graphics.hpp>

#include "image_manager.hpp"
#include "world.hpp"

class Renderer
{
    public:

    Renderer();

    sf::RenderWindow* rW(){return &rW_;}

    void set_bitdepth(unsigned short bitdepth){bitdepth_ = bitdepth;}
    void set_width_height(int width, int height);
    void set_title(std::string title){title_ = title;}
    void set_vsync(bool vsync){vsync_ = vsync;}

    void Quit();
    void UpdateWindow();
    void Render();
    void RenderWorld();
    void RenderUI();
    //Just sets the flag, the actual screenshot is taken later.
    void Screenshot(){need_screenshot_ = true;}


    private:

    bool need_screenshot_;
    bool vsync_;
    int width_;
    int height_;
    int bitdepth_;
    std::string title_;

    ImageManager im_;
    sf::View world_view_;
    sf::RenderWindow rW_;

    void TakeScreenshot();
};

#endif
