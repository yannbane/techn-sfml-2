#ifndef TECHN_PLAYER_H_
#define TECHN_PLAYER_H_

#include "object.hpp"

class Player : public Object {
    public:

    Player(Object *tmpl);

    unsigned short health(){return health_;}

    void set_health(unsigned short health){health_ = health;}


    private:

    unsigned short health_;
};

#endif
