#ifndef TECHN_VECTOR2_H_
#define TECHN_VECTOR2_H_

template<class T> class Vector2
{
    public:

    Vector2()
    {
        x_ = 0;
        y_ = 0;
    }

    Vector2(T x, T y)
    {
        x_ = x;
        y_ = y;
    }

    T x() {return x_;}
    T y() {return y_;}

    void set_xy(T x, T y) {x_ = x; y_ = y;}

    void set_x(T x) {x_ = x;}
    void set_y(T y) {y_ = y;}

    void Normalize()
    {
        T tmp = x_ + y_;

        x_ = x_/tmp;
        y_ = y_/tmp;
    }

    void Add(int x, int y)
    {
        x_ += x;
        y_ += y;
    }

    void Add(Vector2 &v)
    {
        this->Add(v.x(), v.y());
    }

    Vector2<T> operator+(const Vector2<T> &rhs)
    {
        return Vector2<T>(x_ + rhs.x_, y_ + rhs.y_);
    }

    Vector2<T> operator-(const Vector2<T> &rhs)
    {
        return Vector2<T>(x_ - rhs.x_, y_ - rhs.y_);
    }

    void operator+=(const Vector2<T> &other)
    {
        x_ += other.x_;
        y_ += other.y_;
    }

    void operator-=(const Vector2<T> &other)
    {
        x_ += other.x_;
        y_ += other.y_;
    }


    private:

    T x_;
    T y_;
};

#endif
