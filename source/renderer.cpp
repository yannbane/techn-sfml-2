#include "renderer.hpp"

#include <algorithm>
#include <iostream>

#include "twindow.hpp"
#include "object.hpp"
#include "world.hpp"
#include "tinclude.hpp"

Renderer::Renderer(Twindow *window) :
    need_screenshot_(false),
    window_needs_update_(false),
    window_(window)
{
    world_view_.setCenter(0.f, 0.f);
}

void Renderer::Update() {
    world_view_.setSize(window_->getSize().x, window_->getSize().y);
}

bool behind(Object *ob1, Object *ob2) {
    if (ob1->layer() >= ob2->layer()) {
        return false;
    }

    return true;
}


void Renderer::Sort() {
    std::sort(world_->objects()->begin(), world_->objects()->end(), behind);
}

void Renderer::RenderWorld() {
    window_->setView(world_view_);
    //Get sky color from world.
    window_->clear(sf::Color(200*0.9, 215*0.9, 255*0.9));

    for (int i = 0; i < world_->NumObjects(); ++i) {
        window_->draw(*(world_->objecti(i)->sprite()));
    }
}

void Renderer::RenderUI() {
    window_->setView(window_->getDefaultView());
}

void Renderer::Render() {
    static unsigned short counter = 0;

    //Sort each 10 frames:
    if (counter < 10) {
        ++counter;
    }
    else {
        //Sort();
        counter = 0;
    }

    window_->setView(window_->getDefaultView());
    window_->clear();

    RenderWorld();
    RenderUI();

    window_->display();

    if (need_screenshot_) {
        window_->TakeScreenshot();
        need_screenshot_ = false;
    }
}
