#include "object.hpp"

#include <stdexcept>

#include "tinclude.hpp"

Object::Object() :
    update_flags_(0),
    angle_(0.f),
    density_(1.f),
    position_(0.f, 0.f),
    //name("a"),
    shape_(BOX),
    half_width_(1.f),
    half_height_(1.f),
    type_(b2_dynamicBody),
    layer_(0),
    body_(nullptr)
{

}

void Object::UpdateSprite() {
    if (body_ == nullptr) {
        throw std::runtime_error("Object::UpdateSprite: \"Object not initialized.\" (in file \"object.cpp\").");
    }

    b2Vec2 position = body_->GetPosition();
    sprite_.setPosition(position.x*25, position.y*25);

    float old_angle = sprite_.getRotation();
    float new_angle = -body_->GetAngle()*360/2/3.14;
    sprite_.rotate(new_angle - old_angle);
}

void Object::set_angle(float angle) {
    angle_ = angle;
    update_flags_ |= 0b10000000;
}

void Object::set_density(float density) {
    density_ = density;
    update_flags_ |= 0b01000000;
}

void Object::set_type(b2BodyType type) {
    type_ = type;
    update_flags_ |= 0b00001000;
}

void Object::set_position(const b2Vec2& position) {
    position_ = position;
    update_flags_ |= 0b00100000;
}

void Object::SetAsBox(float half_width, float half_height) {
    half_width_ = half_width;
    half_height_ = half_height;
    shape_ = BOX;
    update_flags_ |= 0b00010000;
}

void Object::set_image(sf::Texture* image) {
    sf::Vector2u oldSize = sprite_.getTexture()->getSize();
    sprite_.setTexture(*image);
    sprite_.move((sprite_.getTexture()->getSize().x - oldSize.x)/2, (sprite_.getTexture()->getSize().y - oldSize.y)/2);
}
