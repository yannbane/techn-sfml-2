/* The player can weld blocks together.
 * Blocks must be rectangles.
 * Advanced Transport Laboratory
 */

#include "game.hpp"

#include <Box2D/Box2D.h>

#include "object.hpp"
#include "player.hpp"
#include "tinclude.hpp"

Game::Game() :
    renderer_(&window_)
{
}

void Game::Init() {
    running_ = true;

    //Create the window, set options etc.
    window_.set_title("Window1");
    window_.set_width_height(800, 600);
    window_.set_bitdepth(32);
    window_.set_vsync(true);
    //window_.set_style(sf::Style::Close);
    window_.UpdateWindow(); //Apply above settings.

    renderer_.Update(); //Set halfWidth and halfHeight.
    renderer_.set_world(&world_);

    LoadBasic();
    LoadUI();

    //Watch the states! This shouldn't happen here, UI first.
    CreateWorld("worlds/a.tw");
}

void Game::Quit() {
    window_.close();
    running_ = false;
}

void Game::LoadUI() {
}

void Game::LoadBasic() {
    im_.LoadImage("res/images/boxes/box_wood_covered.png", "box");
    im_.LoadImage("res/images/boxes/box_green.png", "box");
    im_.LoadImage("res/images/walls/blue.png", "blue_wall");
    im_.LoadImage("res/images/misc/ground.png", "ground");
}

void Game::CreateWorld(std::string filename) {
    //Load the world along with all objects:
    wm_.Load(filename, &world_, &im_);

    //Get the object that will serve as a template for the player:
    Object *player = world_.objectn("player");
    world_.RemoveObjectn("player"); //The world no longer needs to know about the template.
    //Create the player from a template object:
    player_ = new Player(player);
    //The template is no longer needed:
    delete player;

    //Set up the player object:
    sf::Texture *image = im_.GetImage("box");
    player_->set_image(image);
    player_->set_position(b2Vec2(0.f, -5.f)); //Horrible, change ASAP.
    player_->SetAsBox(1.f, 31.f/50.f);

    Object *object = new Object;
    object->set_type(b2_staticBody);
    object->set_image(im_.GetImage("ground"));
    object->set_position(b2Vec2(0.f, 10.f)); //Horrible, change ASAP.
    object->SetAsBox(20.f, 1.f);


    int n_objects = 200;

    for (int i = 0; i < n_objects; ++i) {
        sf::Texture *image = im_.GetImage("box");
        Object *object = new Object;
        object->set_image(image);
        object->set_type(b2_dynamicBody);
        object->set_position(b2Vec2(10 - rand()%21, -(rand()%101)));//b2Vec2(sf::Randomizer::Random(-10.f, 10.f), sf::Randomizer::Random(-100.f, 0.f))); //Horrible, change ASAP.
        object->SetAsBox(1.f, 31.f/50.f);
        world_.AddObject(object);
    }

    for (int i = 0; i < n_objects; ++i) {
        sf::Texture *image = im_.GetImage("blue_wall");
        Object *object = new Object;
        object->set_image(image);
        object->set_type(b2_dynamicBody);
        object->set_position(b2Vec2(10 - rand()%21, -(rand()%101)));//         b2Vec2(sf::Randomizer::Random(-10.f, 10.f), sf::Randomizer::Random(-100.f, 0.f))); //Horrible, change ASAP.
        object->SetAsBox(0.5f, 0.5f);
        world_.AddObject(object);
    }

    //Add the player object to the world:
    world_.AddObject(player_);
    world_.AddObject(object);

    /* 1. Read some .ini file
     * 2. Set player stats.
     */
}

void Game::HandleEvents() {
    sf::Event event;

    while (window_.pollEvent(event)) {
        if (event.type == sf::Event::Closed) {
            Quit();
        }

        if ((event.type == sf::Event::KeyPressed) && \
            (event.key.code == sf::Keyboard::F1)) {
            renderer_.Screenshot();
        }

        if (event.type == sf::Event::Resized) {
            renderer_.Update();
        }
    }
}

void Game::Update() {    
    sf::Time dt = deltaClock_.restart();
    world_.Update(dt.asMilliseconds()); //Positions are updated.
}
