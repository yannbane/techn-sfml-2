#ifndef TECHN_RENDERER_H_
#define TECHN_RENDERER_H_

#include <string>

#include <SFML/Graphics.hpp>

#include "image_manager.hpp"

class World;
class Twindow;

class Renderer {
    public:

    Renderer(Twindow *window);

    void set_window(Twindow *window){window_ = window;}
    void set_world(World *world){world_ = world;}

    void Render();
    void RenderWorld();
    void RenderUI();
    void Screenshot(){need_screenshot_ = true;}
    //Resizes the renderer with the correct ratio:
    void Update();

    void Move();
    void Zoom();


    private:

    bool need_screenshot_;
    bool window_needs_update_;

    Twindow *window_;
    World *world_; //Set by the Game.
    ImageManager im_;
    sf::View world_view_;

    void Sort(); //Sorts by layers.
};

#endif
