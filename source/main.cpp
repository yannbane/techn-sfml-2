#include "game.hpp"

int main()
{
    Game *game = new Game;

    game->Init();

    while (game->running())
    {
        game->HandleEvents();
        game->Update();
        game->Render();
    }

    return 0;
}
