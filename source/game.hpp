#ifndef TECHN_GAME_H_
#define TECHN_GAME_H_

#include "twindow.hpp"
#include "renderer.hpp"
#include "image_manager.hpp"
#include "world_manager.hpp"
#include "world.hpp"

class Player;

class Game {
    public:

    Game();

    bool running(){return running_;}

    void Init();
    void HandleEvents();
    void Update();
    void Render(){renderer_.Render();}


    private:

    bool running_;

    Twindow window_; //sf::RenderWindow facade.
    sf::Clock deltaClock_;
    Renderer renderer_; //Rendering logic.
    ImageManager im_;
    WorldManager wm_;
    World world_;
    //This is a pointer only because it is later retrieved from world_.
    Player *player_;

    void Quit();
    void LoadBasic(); //Load essential assets.
    void LoadUI(); //Load assets needed for rendering the UI.
    //Read a save file and create a world from it.
    void CreateWorld(std::string filename); //Sets the player_ pointer.
};

#endif
