#ifndef TECHN_WORLDMANAGER_H_
#define TECHN_WORLDMANAGER_H_

#include <string>

class World;
class ImageManager;

class WorldManager
{
    public:

    void Load(std::string filename, World *world, ImageManager *im);
};

#endif
