#include "world.hpp"

#include <stdexcept>

#include "object.hpp"
#include "tinclude.hpp"

World::World() :
    gravity_(0.f, 10.f),
    world_(gravity_),
    velocity_it_(8),
    position_it_(3)
{
    bool doSleep = true;
    world_.SetAllowSleeping(doSleep);
}

void World::Update(float dt)
{
    for (unsigned int i = 0; i < objects_.size(); ++i)
    {
        if (objects_[i]->update_flags_ != 0) {
            UpdateObject(objects_[i]);
        }
    }

    world_.Step(dt, velocity_it_, position_it_);

    for (unsigned int i = 0; i < objects_.size(); ++i)
    {
        Object* ob = objects_[i];

        ob->UpdateSprite();
    }
}

void World::UpdateObject(Object *ob) {
    //Angle needs to be updated:
    if (ob->update_flags_ && 0b100000000) {
        ob->body()->SetTransform(ob->body()->GetPosition(), ob->angle());
    }

    ob->update_flags_ = 0;
}

void World::PrepareObject(Object *ob) {
    b2BodyDef def;

    //Set the definition up:
    def.position =  ob->position();
    def.angle    =  ob->angle();
    def.type     =  ob->type();

    //Create the body in the world:
    b2Body* body = world_.CreateBody(&def);


    b2PolygonShape shape;

    switch (ob->shape()) {
        case Object::BOX:
            shape.SetAsBox(ob->half_width(), ob->half_height());
            break;
    }

    body->CreateFixture(&shape, ob->density());

    //The only alteration to the object:
    ob->set_body(body);
    ob->update_flags_ = 0b00000000; //Everything has been updated.
}

/* Search for deleted objects (null pointers), and use them (recycle).
 * Otherwise, if no deleted objects were found, increase the vector and
 * add a new pointer. */
void World::AddObject(Object *ob)
{
    if (ob == nullptr)
    {
        throw std::runtime_error("Can't add 'nullptr' to world");
    }

    PrepareObject(ob);

    for (unsigned int i = 0; i < objects_.size(); ++i)
    {
        if (objects_[i] == nullptr)
        {
            objects_[i] = ob;
            return;
        }
    }

    objects_.push_back(ob);
}

//objecti implemented in the header
Object *World::objectn(std::string name) {
    for (unsigned int i = 0; i < objects_.size(); ++i) {
        if (objects_[i] != nullptr) {
            if (objects_[i]->name() == name) {
                return objects_[i];
            }
        }
    }

    return nullptr;
}

void World::RemoveObjecti(int index)
{
    objects_[index] = nullptr;
}

void World::DeleteObjecti(int index)
{
    delete objects_[index];
    objects_[index] = nullptr;
}

//Removes all objects with a certain name, not just the first one:
void World::RemoveObjectn(std::string name)
{
    for (unsigned int i = 0; i < objects_.size(); ++i)
    {
        if (objects_[i] != nullptr)
        {
            if (objects_[i]->name() == name)
            {
                objects_[i] = nullptr;
            }
        }
    }
}

//Only use on objects created on the heap with 'new'.
void World::DeleteObjectn(std::string name)
{
    for (unsigned int i = 0; i < objects_.size(); ++i)
    {
        if (objects_[i] != nullptr)
        {
            if (objects_[i]->name() == name)
            {
                delete objects_[i];
                objects_[i] = nullptr;
            }
        }
    }
}
