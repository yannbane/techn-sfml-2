#ifndef TECHN_OBJECT_H_
#define TECHN_OBJECT_H_

//Box is 50x31

#include <vector>

#include <Box2D/Box2D.h>
#include <SFML/Graphics.hpp>

class b2Body;

class b2Vec2;
class Object {
    public:

    enum Shape {
        BOX/*,
        CRICLE,
        POLYGON*/
    };

    Object();

    std::string     name(){return name_;}
    float           angle(){return angle_;}
    float           density(){return density_;}
    b2BodyType      type(){return type_;}
    Shape           shape(){return shape_;}
    float           half_width(){return half_width_;}
    float           half_height(){return half_height_;}
    b2Body*         body(){return body_;}
    sf::Sprite*     sprite(){return &sprite_;}
    unsigned short  layer(){return layer_;}
    b2Vec2          position(){return position_;}

    void set_name(std::string name){name_ = name;}
    void set_angle(float angle);
    void set_density(float density);
    void set_type(b2BodyType type);
    void set_position(const b2Vec2 &position);
    void set_image(sf::Texture* image);
    void set_layer(unsigned short layer){layer_ = layer;}
    void set_body(b2Body* body){body_ = body;}

    void SetAsBox(float half_width, float half_height);
    //void SetAsCircle();
    //void SetAsChain();

    //world -> screen
    void UpdateSprite();

   //angle, density, position, shape, type
    char update_flags_;


    private:

    float angle_;
    float density_;
    b2Vec2 position_; //Used only for the construction of the body.
    std::string name_;

    //Enumerated properties and their arguments:
    Shape shape_;
    float half_width_;
    float half_height_;

    b2BodyType type_;
    unsigned short layer_; //Lower gets rendered (only matters for rendering).
    sf::Sprite sprite_; //One image per Object
    b2Body* body_;
};
#endif
