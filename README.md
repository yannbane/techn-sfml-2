Tech.N();
=====

A game about building a technological utopia from scratch.

In essence, similar to Terraria, but with a greater emphasis on non-blocky terrain, futuristic technology and physics (flight, explosions, etc).

The day-night cycles are familiar: at day you develop your base, craft weapons or explore, and at night, various creatures storm your fortress in waves.

Unlike Minecraft or Terraria, Tech.N(); features progress in form of missions and objectives (all of which are beneficial, but optional).

The player controls an avatar who has escaped to an island to build his empire on, assisted by robots, drones and AI. It is possible to  confront other civilizations.
