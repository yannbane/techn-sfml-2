CC=g++
CFLAGS=-Wall -std=c++0x

INSTPATH=/opt/Techn

OBJ=object/game.o object/twindow.o object/renderer.o object/world.o object/player.o object/object.o object/image_manager.o object/world_manager.o

all : bin/techn

bin/techn : source/main.cpp $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) $< -o $@ -lsfml-graphics -lsfml-window -lsfml-system -lBox2D

object/object.o : source/object.cpp source/object.hpp
	$(CC) $(CFLAGS) -c $< -o $@

object/player.o : source/player.cpp source/player.hpp object/object.o
	$(CC) $(CFLAGS) -c $< -o $@

object/image_manager.o : source/image_manager.cpp source/image_manager.hpp
	$(CC) $(CFLAGS) -c $< -o $@

object/world_manager.o : source/world_manager.cpp source/world_manager.hpp object/object.o
	$(CC) $(CFLAGS) -c $< -o $@

object/world.o : source/world.cpp source/world.hpp object/object.o object/world_manager.o
	$(CC) $(CFLAGS) -c $< -o $@

object/game.o : source/game.cpp source/game.hpp object/player.o object/renderer.o object/world.o object/world_manager.o object/twindow.o
	$(CC) $(CFLAGS) -c $< -o $@

object/renderer.o : source/renderer.cpp source/renderer.hpp object/twindow.o object/object.o object/image_manager.o object/world.o
	$(CC) $(CFLAGS) -c $< -o $@

object/twindow.o : source/twindow.cpp source/twindow.hpp
	$(CC) $(CFLAGS) -c $< -o $@

clean :
	rm -f bin/* object/*
